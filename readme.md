Proyecto para probar distintos conceptos y bibliotecas de HTML y JavaScript.
##Consideraciones:
	* Existe una carpeta por cada concepto/biblioteca.
	* Cada una de esas carpetas tiene su index.html y es independiente y autocontenido.
	* Para mayor comodidad, son proyectos hechos con Netbeans, no es necesario un apache.
